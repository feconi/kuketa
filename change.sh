#!/bin/bash

if [ -z "$1" ]; then
  echo "Usage: ./change presentationname"
fi

if [ ! -d "reveal.js" ]; then
  echo "./reveal.js/ not found\nuse ./make.sh to install first"
  exit 1
fi

cp "$1.html" reveal.js/index.html

if [ -d assets/$1 ]; then
  cp -r "assets/$1/." reveal.js/assets/
fi

# cp -r assets/ reveal.js/assets/
