#!/bin/bash

if [ ! -d "reveal.js" ]; then
    echo -e "\e[1m== Cloning reveal.js from github\e[0m"
    git clone https://github.com/hakimel/reveal.js.git
    echo -e "\e[1m== Done\e[0m"
fi

