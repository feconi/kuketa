# KuKeTa
KuschelKeksTalks

Hier gibt es eine Menge an Vorträgen und ein Vortragsverwaltungssystem.
Die Vorträge sind mit reveal.js (link) gebaut.
Alle Vorträge sind zu Fortbildungs- und Aufklärungszwecken erstellt.
Außerdem sollen sie Freude und Spaß verbreiten.

## Installation

Zur Installation kann **make.sh** verwendet werden.
Es kann optional mit angegeben werden, welcher Vortrag aktiviert werden soll.
```
./make.sh {example}
```

In diesem Repo vorhandene Vortraege sind:
* murmeln
* jsfuck
* reton


### Manuelle Installation:

1. Downloade reveal.js
```
git clone https://github.com/hakimel/reveal.js.git
```
2. Ersetze die index.html von reveal.js durch eine .html aus diesem Repo
```
cp {example}.html reveal.js/index.html
```
3. Kopiere die assets
```
cp -r assets/ reveal.js/
```

Jetzt kann die Praesentation genutzt werden, indem **reveal.js/index.html** in einem Browser geoeffnet wird.

### Vortragswechsel

Um einen Vortrag zu aktivieren kann das Script **make.sh** verwendet werden.
```
./make {example}
```

Ist bereits ein anderer Vortrag aktiviert, wird dieser einfach ersetzt.
Siehe **Schritt 2 und 3** aus der **Manuellen Installation**.

## Slides

Alle Slides der Praesentation sind in der **index.html** enthalten.
Jede Slide wird durch ein `<section>` repraesentiert.

Fuer Details lies das Readme von reveal.js.
https://github.com/hakimel/reveal.js

## Nutzungsanleitung

Mit den Pfeiltasten kann durch die Praesentation navigiert werden.
Mit Esc kann in eine Slide-overview-Ansicht gewechselt werden.
Mit F kann in den Vollbildmodus gewechselt werden.

Fuer Details schau im Readme von reveal.js nach.
https://github.com/hakimel/reveal.js.git

## Vortraege

* jsfuck
* murmeln
* reton

### jsfuck

Ein Vortrag ueber eine kreative Art javascript zu programmieren, mit der viele Websiten wie ebay gehackt werden koennen.
Dieser Vortrag beinhaltet auf den Slides ausfuehrbare Codebloecke.

Vortragsdauer: 42 Minuten

### murmeln

Ein Vortrag ueber die Sicherheitsluecken in einem Spiel, was im Stud.IP der Uni Goettingen gespielt werden kann.

Vortragsdauer: 15 Minuten

### reton

Ein Vortrag über ein Pfandbonsystem von tegut-Filialen.

Vortragsdauer: 42 Minuten

### Geplante Vortraege

* Verschluesselung
* Introduction into neural networks with keras
* pandoc
* Probleme in polyamoren Beziehungen mit Graphentheorie loesen

## License

Alle Dateien, außer einzelne assets, stehen unter Beerware Lizenz.

> ----------------------------------------------------------------------------
> "THE BEER-WARE LICENSE" (Revision 42):
> <feconi@posteo.de> wrote this thing. As long as you retain this notice you
> can do whatever you want with this stuff. If we meet some day, and you think
> this stuff is worth it, you can buy me a beer in return - Felix Schelle
> ----------------------------------------------------------------------------

### assets

* /assets/reton/2214.png, /assets/reton/2714.png

> Bild- und Videoquelle: tegut…

* alle xkcds

> xkcd.com
